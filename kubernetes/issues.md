# Issues

## Version 1.21.10
### Symptoms
- Calico kept getting unauthorized in error logs, no pods could start because of sandbox errors
### Resolution
- Upgraded to 1.22.7

## OIDC Setup
### Symptoms
- Unable to get OIDC Discovery
### "Resolution"
- Allow annoymous auth and setup clusterrole to allow access on the discovery path
- Not ideal, need to find better option, appears others use S3 buckets to host the data, something authenticates and uploads to s3 the data?
