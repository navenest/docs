GPG_EMAIL=pulp@pineapplenest.com
cat >/tmp/gpg.txt <<EOF
%echo Generating a basic OpenPGP key
Key-Type: DSA
Key-Length: 1024
Subkey-Type: ECDSA
Subkey-Curve: nistp256
Name-Real: Collection Signing Service
Name-Comment: with no passphrase
Name-Email: $GPG_EMAIL
Expire-Date: 0
%no-ask-passphrase
%no-protection
# Do a commit here, so that we can later print "done" :-)
%commit
%echo done
EOF
gpg --batch --gen-key /tmp/gpg.txt
gpg --export-secret-keys -a pulp@pineapplenest.com  > /tmp/gpg_private_key.gpg
gpg --export -a pulp@pineapplenest.com > /tmp/gpg_armor.asc
kubectl create secret generic signing-secret -n pulp --from-file=signing_service.gpg=/tmp/gpg_private_key.gpg --from-file=signing_service.asc=/tmp/gpg_armor.asc

SIGNING_SCRIPT_PATH=/tmp
COLLECTION_SIGNING_SCRIPT=collection_script.sh
cat<<EOF> "$SIGNING_SCRIPT_PATH/$COLLECTION_SIGNING_SCRIPT"
#!/usr/bin/env bash
set -u
FILE_PATH=\$1
SIGNATURE_PATH="\$1.asc"

ADMIN_ID="\$PULP_SIGNING_KEY_FINGERPRINT"
PASSWORD="password"

# Create a detached signature
gpg --quiet --batch --pinentry-mode loopback --yes --passphrase \
   \$PASSWORD --homedir ~/.gnupg/ --detach-sign --default-key \$ADMIN_ID \
   --armor --output \$SIGNATURE_PATH \$FILE_PATH

# Check the exit status
STATUS=\$?
if [ \$STATUS -eq 0 ]; then
   echo {\"file\": \"\$FILE_PATH\", \"signature\": \"\$SIGNATURE_PATH\"}
else
   exit \$STATUS
fi
EOF

CONTAINER_SIGNING_SCRIPT=container_script.sh
cat<<EOF> "$SIGNING_SCRIPT_PATH/$CONTAINER_SIGNING_SCRIPT"
#!/usr/bin/env bash
set -u

MANIFEST_PATH=\$1
IMAGE_REFERENCE="\$REFERENCE"
SIGNATURE_PATH="\$SIG_PATH"

skopeo standalone-sign \
      \$MANIFEST_PATH \
      \$IMAGE_REFERENCE \
      \$PULP_SIGNING_KEY_FINGERPRINT \
      --output \$SIGNATURE_PATH

# Check the exit status
STATUS=\$?
if [ \$STATUS -eq 0 ]; then
  echo {\"signature_path\": \"\$SIGNATURE_PATH\"}
else
  exit \$STATUS
fi
EOF

DEBIAN_SIGNING_SCRIPT=debian_script.sh
cat<<EOF> "$SIGNING_SCRIPT_PATH/$DEBIAN_SIGNING_SCRIPT"
#!/usr/bin/env bash
set -e

FILE_PATH=\$1
OUTPUT_DIR=\$1
DETACHED_SIGNATURE_PATH="\$OUTPUT_DIR/Release.gpg"
INLINE_SIGNATURE_PATH="\$OUTPUT_DIR/InRelease"
GPG_KEY_ID="Pulp QE"
COMMON_GPG_OPTS="--batch --armor --digest-algo SHA256"

# Create a detached signature

gpg --quiet --batch --digest-algo SHA256 --pinentry-mode loopback --yes --passphrase \
   \$PASSWORD --homedir ~/.gnupg/ --detach-sign --default-key \$ADMIN_ID \
   --armor --output \$DETACHED_SIGNATURE_PATH \$FILE_PATH

# Create an inline signature

gpg --quiet --batch --digest-algo SHA256 --pinentry-mode loopback --yes --passphrase \
   \$PASSWORD --homedir ~/.gnupg/ --inline --default-key \$ADMIN_ID \
   --armor --output \$INLINE_SIGNATURE_PATH \$FILE_PATH


# Check the exit status
STATUS=\$?
if [ \$STATUS -eq 0 ]; then
  echo { \
       \"signatures\": { \
         \"inline\": \"\$INLINE_SIGNATURE_PATH\", \
         \"detached\": \"\$DETACHED_SIGNATURE_PATH\" \
       } \
     }
else
  exit \$STATUS
fi
EOF
kubectl delete secret signing-scripts -n pulp
kubectl create secret generic signing-scripts -n pulp --from-file=collection_script.sh=/tmp/collection_script.sh --from-file=container_script.sh=/tmp/container_script.sh --from-file=debian_script.sh=/tmp/debian_script.sh