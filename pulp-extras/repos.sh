

NAME='debian'
SIGNING_SERVICE_NAME='collection-signing-service'
REMOTE_OPTIONS=(
  --url=http://deb.debian.org/debian
  --distribution=bookworm
  --component=main
  --component=non-free-firmware
  --architecture=amd64
  --policy=on_demand
)
pulp deb remote create --name=${NAME} ${REMOTE_OPTIONS[@]}
pulp deb repository create --name=${NAME} --remote=${NAME}
pulp deb repository sync --name=${NAME}
pulp deb publication create --repository=${NAME}
pulp deb distribution create --name=${NAME} --base-path=${NAME} --repository=${NAME}

NAME='debian-security'
SIGNING_SERVICE_NAME='collection-signing-service'
REMOTE_OPTIONS=(
  --url=http://security.debian.org/debian-security
  --distribution=bookworm-security
  --component=main
  --component=non-free-firmware
  --architecture=amd64
  --policy=on_demand
)
pulp deb remote create --name=${NAME} ${REMOTE_OPTIONS[@]}
pulp deb repository create --name=${NAME} --remote=${NAME}
pulp deb repository sync --name=${NAME}
pulp deb publication create --repository=${NAME}
pulp deb distribution create --name=${NAME} --base-path=${NAME} --repository=${NAME}

NAME='debian-updates'
SIGNING_SERVICE_NAME='collection-signing-service'
REMOTE_OPTIONS=(
  --url=http://deb.debian.org/debian
  --distribution=bookworm-updates
  --component=main
  --component=non-free-firmware
  --architecture=amd64
  --policy=on_demand
)
pulp deb remote create --name=${NAME} ${REMOTE_OPTIONS[@]}
pulp deb repository create --name=${NAME} --remote=${NAME}
pulp deb repository sync --name=${NAME}
pulp deb publication create --repository=${NAME}
pulp deb distribution create --name=${NAME} --base-path=${NAME} --repository=${NAME}

